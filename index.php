<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Uploads</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col"><h1>Upload files</h1></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col">
            <form action="/" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="files">select files</label>
                    <input type="file" name="files[]" multiple>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Upload!">
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>


<?php require './vendor/autoload.php'; ?>

<?php


foreach ($_FILES['files']['error'] as $key => $error) {
    var_dump($_FILES['files']['tmp_name'][$key]);
    echo '<hr>';
}

define('SG_MAIL_HOST', 'smtp.yandex.ru');
define('SG_MAIL_SMTP_SECURE', 'ssl');
define('SG_MAIL_PORT', '465');
define('SG_MAIL_USERNAME', '');
define('SG_MAIL_PASSWORD', '');

/**
 * @return bool
 * @throws \PHPMailer\PHPMailer\Exception
 */
function sendMail()
{
    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

    $mail->isSMTP();
    $mail->Host = SG_MAIL_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SG_MAIL_USERNAME;
    $mail->Password = SG_MAIL_PASSWORD;
    $mail->SMTPSecure = SG_MAIL_SMTP_SECURE;
    $mail->Port = SG_MAIL_PORT;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom(SG_MAIL_USERNAME, 'Svarog');
    $mail->addAddress('artem.suvorkin@mail.ru');
    $mail->Body = 'Test upload files';
    $mail->Subject = 'Test upload files';

    foreach ($_FILES['files']['error'] as $key => $error) {
        $tmpName = $_FILES['files']['tmp_name'][$key];
        $fileName = $_FILES['files']['name'][$key];

        $mail->addAttachment($tmpName, $fileName);
    }

    $mail->send();
}


if(isset($_POST)) {
    sendMail();
}

?>
